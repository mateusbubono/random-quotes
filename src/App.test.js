import React from 'react';
import { render } from '@testing-library/react';
import App from './App';
import RandomQuote from './App';
import { unmountComponentAtNode } from "react-dom";
import ReactDOM from "react-dom"

/*test('renders learn react link', () => {
  const { getByText } = render(<App />);
  const linkElement = getByText(/learn react/i);
  expect(linkElement).toBeInTheDocument();
});*/

describe("test RandomQuote => Citation", () => {
  
  test("La citation doit être présente", () => {
    const randomQuote = render(<RandomQuote />)
    const articleElement = randomQuote.getByRole("article")
    const pElement = articleElement.getElementsByTagName("p")[0]
    console.log(pElement.innerHTML)
    expect(pElement.innerHTML).not.toBe("") 
    expect(pElement.innerHTML).not.toBe(null)  
    expect(pElement.innerHTML).not.toBe(undefined) 
  })

  test("L'auteur de la citation doit être présent", () => {
    const randomQuote = render(<RandomQuote />)
    const authorDivElement = randomQuote.getByTestId("author")
    console.log(authorDivElement.innerHTML)
    expect(authorDivElement.innerHTML).not.toBe("") 
    expect(authorDivElement.innerHTML).not.toBe(null)  
    expect(authorDivElement.innerHTML).not.toBe(undefined) 
  })

})


