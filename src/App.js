import React from 'react';
import $ from 'jquery';
import './App.css';


export class RandomQuote extends React.Component 
{
  constructor(props, state)
  {
    super(props, state)
    this.state = {
      //Object contenant les citations avec en clé l'auteur et en valeur la citation
      quoteObject : {
        "Edmund Burke" : "Ceux qui ont beaucoup à espérer et rien à perdre seront toujours dangereux.",
        "Louis Pauwels" :"Seule la paresse fatigue le cerveau.",
        "Jules César" : "L’expérience, voilà le maître en toutes choses.",
        "Albert Einstein" : "La vie, c’est comme une bicyclette, il faut avancer pour ne pas perdre l’équilibre.",
        "Paul Morand" : "Que de temps perdu à gagner du temps !",
        "Liza Minnelli" : "La réalité est une chose au-dessus de laquelle il faut s’élever.",
        "Mark Twain" : "Ils ne savaient pas que c’était impossible, alors ils l’ont fait.",
        "Boris Vian" : "Une sortie, c'est une entrée que l'on prend dans l'autre sens.",
        "Sharon Stone" : "Le plaisir est fugitif, pas le bonheur.",
        "Juliette Binoche" : "On ne peut rêver que si on a les pieds sur terre. Plus les racines sont profondes, plus les branches sont porteuses."
      },
      //Table contenant l'ensemble des couleurs qui seront affichée par le générateur de citation
      colorObject : [
        "#11d131",
        "#10866d",
        "#fc0339",
        "#ca03fc",
        "#fc9d03", 
        "gray"
      ],
      //Contient l'auteur actuellement affiché dans le générateur
      author : "Edmund Burke",
      //Contient la coumeur actuellement affichée dans le générateur
      color : "gray",
      //Ce flag permet de déclencher une animation dans la fonction changeQuote
      bAlternateAnim: false,
    }

    //Cette fonction est appelé lorsque l'on clique sur le bouton "Nouvelle citation"
    this.changeQuote = this.changeQuote.bind(this)
  }

  /**
   * Fonction selectElementRandomly : La fonction prend un tableau en paramètre et en retourne un élément sélectionné au hasard
   * 
   * @param {array} arrayToCheck : tableau utilisé en entré.
   * 
   * @return {string|any} : La fonction retourne un élément du tableau sélectionné au hasard. En principe il s'agit d'une string mais cela pourrait être tout type données
   */
  selectElementRandomly(arrayToCheck)
  {
    const randomIndex = Math.round(Math.random() * (this.state.colorObject.length - 1));

    return arrayToCheck[randomIndex]
  }

  /**
   * Fonction changeQuote : La fonction est appelée quand l'utilisateur clique sur le bouton "Nouvelle citation". Elle va ainsi :
   *  - Sélectionner une nouvelle couleur qui va s'afficher dans le fond, comme couleur de bouton et comme couleur de texte pour la citation
   *  - Régler et activer une animation de transition vers la citation suivante qui consiste à un changement d'opacité du texte et une transition de la couleur actuelle vers une nouvelle couleur
   *  - Mettre à jour la propriété state pour qu'elle contienne l'auteur et la couleur sélectionnée
   */
  changeQuote()
  {
    /*
    On commence par sélectionner une couleurs au hasard dans la table colorObject de l'attribut state. Ensuite on va sélectionner
    un auteur dans l'objet quoteObject toujours de l'attribut state. Chaque recherche est imbriquée dans une boucle qui va relancer la recherche dans le cas ou la couleur ou l'auteur sélectionné sont les même que le tirage précédent 
    */
    let nextColor = null
    do
    {
      nextColor = this.selectElementRandomly(this.state.colorObject)
    }while(nextColor === this.state.color)

    let nextAuthor = null 
    do
    {
      nextAuthor = this.selectElementRandomly(Object.keys(this.state.quoteObject))
    }while(nextAuthor === this.state.author)
    
    //On va assigner à des variable css une couleur initiale correspondant à la couleur affichée actuellement, puis une couleur vers laquelle on va faire une transition via une animation css
    $(document.documentElement)
      .css("--random-initial-color", this.state.color)
      .css("--random-final-color", nextColor)
    
    //Une fois la transition faite il va falloir que la couleur de fond de la page ainsi que la couleur du bouton soit aligné sur la nouvelle couleur sélectionné, c'est le rôle du code ci-dessous  
    $("#random-quote, #new-quote")
      .css("background-color", nextColor)
      .toggleClass("test-anim") //On ajoute une classe à nos deux éléments pour déclencher l'animation de transition quand le bouton est cliqué
    
    //Pour le texte de citation et l'auteur (ainsi que pour la couleur du logo twitter), on va changer la couleur du texte pour qu'elle corresponde à la nouvelle couleur sélectionnée
    $("#text, #author, #tweet-quote")
      .css("color", nextColor)
      .toggleClass("text-anim")//On ajoute une classe à nos deux éléments pour déclencher l'animation de transition quand le bouton est cliqué

    /*
    Les classes ajoutée test-anim et text-anim sont là pour déclencher l'animation de transition de couleur. Au premier clique on va activer la classe, puis au second on la désactive. L'animation de cette façon ne se lancerait qu'une fois sur deux. C'est pour cela que j'ai ajouté une seconde classe de de transition de couleur test-anim2 et text-anim2. Elle auront pour but de s'activer quand test-anim et text-anim seront désactivé, ce qui permet de jouer l'animation de transition de couleur à chaque clique sur le bouton 
    */

    //Les ligne ci-dessous ne seront exécutée qu'au deuxième clique sur le bouton lorsque le flag bAlternateAnim sera activé
    if(this.state.bAlternateAnim === true)
    {
      $("#random-quote, #new-quote").toggleClass("test-anim2")

      $("#text, #author, #tweet-quote").toggleClass("text-anim2")
    }
    else
    {
      this.setState({
        bAlternateAnim: true
      })
    }

    //La fonction setTimeout utilisée ici est appelée au bout d'une seconde soit la moitié d'un temps d'animation de transition de couleur et de citation. Cela permet à la citation de s'effacer, de changer puis de réapparaître. Sans cette instruction la citation est changée puis l'animation de transition se déclenche par la suite mais trop tard
    setTimeout(() => {
      this.setState({
        author : nextAuthor,
        color : nextColor
      }) 
    }, 1000)
  }

  render()
  {
    // eslint-disable-next-line
    const anchorElement = (<a href="twitter.com/intent/tweet"><svg aria-hidden="true" focusable="false" data-prefix="fab" data-icon="twitter-square" class="svg-inline--fa fa-twitter-square fa-w-14" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><path fill={this.state.color} d="M400 32H48C21.5 32 0 53.5 0 80v352c0 26.5 21.5 48 48 48h352c26.5 0 48-21.5 48-48V80c0-26.5-21.5-48-48-48zm-48.9 158.8c.2 2.8.2 5.7.2 8.5 0 86.7-66 186.6-186.6 186.6-37.2 0-71.7-10.8-100.7-29.4 5.3.6 10.4.8 15.8.8 30.7 0 58.9-10.4 81.4-28-28.8-.6-53-19.5-61.3-45.5 10.1 1.5 19.2 1.5 29.6-1.2-30-6.1-52.5-32.5-52.5-64.4v-.8c8.7 4.9 18.9 7.9 29.6 8.3a65.447 65.447 0 0 1-29.2-54.6c0-12.2 3.2-23.4 8.9-33.1 32.3 39.8 80.8 65.8 135.2 68.6-9.3-44.5 24-80.6 64-80.6 18.9 0 35.9 7.9 47.9 20.7 14.8-2.8 29-8.3 41.6-15.8-4.9 15.2-15.2 28-28.8 36.1 13.2-1.4 26-5.1 37.8-10.2-8.9 13.1-20.1 24.7-32.9 34z"></path></svg></a>)
    return (
      <div id="random-quote">
        <section id="quote-box" className="border border-dark rounded-right">
          <article id="text">
            <p>
                {this.state.quoteObject[this.state.author]}
            </p>
          </article>
          <div id="author" data-testid="author">
              - {this.state.author}
          </div>
          <button id="new-quote" className="btn btn-default" onClick={this.changeQuote}>
                Nouvelle citation</button>
          
    <div id="tweet-quote">{anchorElement}</div>
        </section>
      </div>
    );
  }
}

function App() {
  return (
    <div className="App">
    <RandomQuote />
  </div>
  );
}


export default App;
