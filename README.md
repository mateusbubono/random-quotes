# Random Quotes

Random Quotes est une application React qui vous affiche une citation aléatoire

## Installation

Vous pouvez voir le résultat ici :

https://mateusbubono.gitlab.io/random-quotes/

Ou vous pouvez cloner le projet en utilisant git clone il vous faudra ensuite npm pour lancer l'application

```bash
git clone https://gitlab.com/mateusbubono/random-quotes.git
cd random-quotes
npm install
```

## Usage

Depuis le répertoire du projet, utilisez npm pour lancer l'application

```bash
npm run start
```
